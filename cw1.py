def func(*args, **kwargs):
    print(f"args: {args}")
    a_sum = 0
    print(f"kwargs: {kwargs}")
    for item in args:
        a_sum += item
    return a_sum

print(func(2,3,4,5,7, v=34, s=43))